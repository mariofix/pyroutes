# pyroutes
Python script to automagicly add or delete routes (using iproute2) via post-up/down (debian) or similar (in other distros)

###Usage
This file can be used in two ways
##### Calling it directly
```sh
$ ./ruteo add archivo
$ ./ruteo del archivo
$ ./ruteo change archivo
```


##### Via if/up-down scripts, in debian

```sh
auto eth0
iface eth0 inet static
        address 192.168.0.2
        netmask 255.255.255.0
        post-up /sbin/ruteo add PATH/rutas_eth0.list
        post-down /sbin/ruteo del PATH/rutas_eth0.list
```
